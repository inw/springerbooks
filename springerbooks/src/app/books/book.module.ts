import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookListComponent } from './book-list.component'
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { BookResolver } from './book-resolver.service';
import { BooksComponent } from './books.component';
import { BookCheckboxSelectionComponent } from './filter/book-checkbox-selection.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookFilterComponent } from './book-filter.component';
import { PipesModule } from '../shared/pipes.module';

import { FlexLayoutModule } from '@angular/flex-layout';

import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatSortModule } from '@angular/material/sort';
import { SearchComponent } from './filter/search.component';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [BooksComponent, BookListComponent, BookCheckboxSelectionComponent, BookFilterComponent, SearchComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    PipesModule,
    RouterModule.forChild([
      { path: 'books',
        component: BooksComponent,
        resolve: {
          resolvedData: BookResolver
        } }
    ]),
    FormsModule,
    MatTableModule,
    MatCheckboxModule,
    MatCardModule,
    MatExpansionModule,
    MatListModule,
    MatGridListModule,
    MatSortModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
  ],
  exports: []
})
export class BookModule { }
