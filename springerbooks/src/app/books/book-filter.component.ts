import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IFilterable } from './filterable';


@Component({
  selector: 'app-book-filter',
  templateUrl: './book-filter.component.html',
  styleUrls: ['./book-filter.component.scss']
})
export class BookFilterComponent {
  @Input() filterables: IFilterable;
  @Output() filterChanged: EventEmitter<IFilterable> = new EventEmitter();

  private curentFilters: IFilterable =  { packages: [], tags: [], languange: [] };
  constructor() { }

  packageFilterChanged($event: string[]) {
    this.curentFilters.packages = $event;
    this.filterChanged.emit(this.curentFilters);
  }

  languageFilterChanged($event: string[]) {
    this.curentFilters.languange = $event;
    this.filterChanged.emit(this.curentFilters);
  }

  tagsFilterChanged($event: string[]) {
    this.curentFilters.tags = $event;
    this.filterChanged.emit(this.curentFilters);
  }
}
