import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() searchChanged: EventEmitter<string> = new EventEmitter();
  titleSearch = new FormControl('');

  constructor() { }

  ngOnInit(): void {
    this.titleSearch.valueChanges
      .pipe(debounceTime(500))
      .subscribe(value => this.textEntered(value))
  }

  textEntered(value: string) {
    this.searchChanged.emit(value);
  }

}
