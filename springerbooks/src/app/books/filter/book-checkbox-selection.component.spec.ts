import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCheckboxSelectionComponent } from './book-checkbox-selection.component';

describe('BookCheckboxSelectionComponent', () => {
  let component: BookCheckboxSelectionComponent;
  let fixture: ComponentFixture<BookCheckboxSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookCheckboxSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCheckboxSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
