import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-book-checkbox-selection',
  templateUrl: './book-checkbox-selection.component.html',
  styleUrls: ['./book-checkbox-selection.component.scss']
})
export class BookCheckboxSelectionComponent {
  @Input() values: string[];
  @Input() title: string;
  @Output() selectionChanged: EventEmitter<string[]> = new EventEmitter();

  checked: Map<string, boolean> = new Map<string, boolean>();
  activeFilterCount: number = 0;

  checkboxChanged($event: MatCheckboxChange): void {
    this.extractCheckboxStatus($event);
    const selectedPackages = this.calculateSelection();
    this.selectionChanged.emit(selectedPackages);
  }

  private extractCheckboxStatus($event: MatCheckboxChange) {
    const checked = $event.checked;
    const packageName = $event.source.name;
    this.checked.set(packageName, checked);
  }

  calculateSelection(): string[] {
    let selection: string[] = [];
    for (let entry of this.checked.entries()) {
      if (entry[1]) {
        selection.push(entry[0])
      }
    }

    if (selection?.length === 0) {
      this.activeFilterCount = 0;
      return this.values;
    }

    this.activeFilterCount = selection.length;
    return selection;
  }

}
