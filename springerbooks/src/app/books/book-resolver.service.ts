import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { IBook } from '../shared/book';
import { Observable } from 'rxjs';
import { BooksService } from '../shared/books.service';

@Injectable({
  providedIn: 'root'
})
export class BookResolver implements Resolve<IBook[]>{

  constructor(private bookService: BooksService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBook[]> {
    return this.bookService.getAll();
  }

}
