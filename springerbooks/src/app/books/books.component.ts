import { Component, OnInit } from '@angular/core';
import { IBook } from '../shared/book';
import { ActivatedRoute } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { IFilterable } from './filterable';
import { LayoutAlignDirective } from '@angular/flex-layout';
import { Observable, BehaviorSubject } from 'rxjs';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  allBooks: IBook[];

  get booksToDisplay(): Observable<IBook[]> {
    return this.selectedBooks.asObservable();
  }

  private selectedBooks: BehaviorSubject<IBook[]> = new BehaviorSubject(this.allBooks);
  filterables: IFilterable =  { packages: [], tags: [], languange: [ 'English' , 'German'] };
  currentFilters: IFilterable;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.allBooks = this.route.snapshot.data['resolvedData'];
    this.calculatePackages(this.allBooks);
    this.calculateTags(this.allBooks);
    this.filterBooks();
  }

  filterBooks(): void {
    let newDisplaySet = this.allBooks;
    if(this.currentFilters?.packages?.length > 0) {
      newDisplaySet = newDisplaySet.filter(book =>this.currentFilters.packages.includes(book.package));
    }
    if(this.currentFilters?.languange?.length > 0) {
      const lang: string[] = [];
      if(this.currentFilters.languange.includes('English')) {
        lang.push('EN');
      }
      if(this.currentFilters.languange.includes('German')) {
        lang.push('DE');
      }
      if (lang.length > 0) {
        newDisplaySet = newDisplaySet.filter(book =>lang.includes(book.lang));
      }
    }
    if(this.currentFilters?.tags?.length > 0) {
      newDisplaySet = newDisplaySet.filter(book => this.currentFilters.tags.some(tag => book.tags.indexOf(tag) >= 0));
    }
    this.selectedBooks.next(newDisplaySet);
  }

  filtersChanged($event: IFilterable) {
    this.currentFilters = $event;
    this.filterBooks();
  }

  calculatePackages(allBooks: IBook[]): void {
    this.filterables.packages = Array.from(new Set(this.allBooks.map(book => book.package)));
  }

  calculateTags(allBooks: IBook[]): void {
    const tags = this.allBooks.map(book => book.tags);
    this.filterables.tags = Array.from(new Set([].concat(...tags)));
  }


}
