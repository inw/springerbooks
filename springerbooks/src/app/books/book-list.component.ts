import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IBook } from '../shared/book';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { SortByPipe } from '../shared/sort-by.pipe';
import { Observable } from 'rxjs';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit{

  @Input() books: Observable<IBook[]>;
  private sortedBooks: IBook[];
  dataSource = new MatTableDataSource();
  displayedColumns = ['title', 'edition', 'package']

  constructor(private sortByPipe: SortByPipe) {

  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit(): void {
    this.books.subscribe(
      books => {
        this.sortedBooks = this.sortByPipe.transform(books, 'asc', 'title');
        this.dataSource = new MatTableDataSource(this.sortedBooks);
        this.dataSource.sort = this.sort;
      }
    )
  }

  filterTitles($event: string) {
    if($event === '') {
      this.dataSource = new MatTableDataSource(this.sortedBooks);
    }
    const filteredBooks = this.sortedBooks.filter(book => book.title.toLowerCase().includes($event.toLowerCase()));
    this.dataSource = new MatTableDataSource(filteredBooks);
  }

}

