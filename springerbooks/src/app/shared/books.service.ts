import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBook } from './book';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private readonly baseUrl = environment.apiBaseUrl;


  constructor(private http: HttpClient) { }

  getAll(): Observable<IBook[]> {
      return this.http.get<IBook[]>(`${this.baseUrl}/api/books`);
  }


}
