"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const fs = require("fs");
const importer_1 = require("./importer");
const app = express();
const converter = new importer_1.Importer();
const resource = `${__dirname}/resources`;
let data = [];
function importData(files) {
    files.forEach(file => {
        data = data.concat(converter.convertToBooks(resource + '/' + file));
    });
    console.log("Holding " + data.length + " title in momory.");
}
app.listen(8181, () => {
    fs.readdir(resource, (err, files) => {
        if (err) {
            console.log(err);
        }
        importData(files);
    });
    console.log('Server listening on 8181');
});
app.use(cors());
app.route('/api/books').get((req, res) => {
    res.send(data);
});
