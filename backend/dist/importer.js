"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const XLSX = require("xlsx");
class Importer {
    convertToBooks(pathToXslx) {
        console.log("Importing " + pathToXslx);
        const workbook = XLSX.readFile(pathToXslx);
        const firstSheetName = workbook.SheetNames.shift();
        const worksheet = workbook.Sheets[firstSheetName];
        const books = [];
        XLSX.utils.sheet_to_json(worksheet)
            .forEach(book => books.push(this.createBook(book)));
        console.log("Imported " + books.length + " titles.");
        return books;
    }
    createBook(book) {
        return {
            title: book['Book Title'],
            edition: book['Edition'],
            type: book['Product Type'],
            year: book['Copyright Year'],
            isbn: book['Print ISBN'],
            eIsbn: book['Electronic ISBN'],
            lang: book['Language'],
            package: book['English Package Name'],
            package_de: book['German Package Name'],
            doi: book['DOI URL'],
            url: book['OpenURL'],
            tags: book['Subject Classification'].split(';'),
            publisher: book['Publisher']
        };
    }
}
exports.Importer = Importer;
