import * as express from 'express';
import * as cors from 'cors';
import * as fs from 'fs';
import { Importer } from './importer';
import { IBook } from './book'

const app = express();
const converter = new Importer();
const resource = `${__dirname}/resources`;
let data: IBook[] = [];

function importData(files: string[]) {    
    files.forEach(file =>  {
           data =  data.concat(converter.convertToBooks(resource+'/'+file));
    });       
    console.log("Holding " + data.length + " title in momory.")
}

app.listen(8181, () => {
    fs.readdir(resource, (err, files) => {
        if(err) {
            console.log(err);
        }
        importData(files);
    });
    console.log('Server listening on 8181');
})

app.use(cors())

app.route('/api/books').get((req, res) => {
    res.send(data);
})
