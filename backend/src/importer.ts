import * as XLSX from 'xlsx';
import { IBook } from './book'

export class Importer {

    convertToBooks(pathToXslx: string): IBook[] {
        console.log("Importing " + pathToXslx);
        const workbook = XLSX.readFile(pathToXslx);
        const firstSheetName =  workbook.SheetNames.shift();
        const worksheet = workbook.Sheets[firstSheetName];
        const books: IBook[] = [];
        XLSX.utils.sheet_to_json(worksheet)
        .forEach(book => 
            books.push(this.createBook(book)));
        console.log("Imported " + books.length + " titles.")
        return books;
    }

    private createBook(book: any) : IBook {
        return {
            title: book['Book Title'],
            edition: book['Edition'],
            type: book['Product Type'],
            year: book['Copyright Year'],
            isbn: book['Print ISBN'],
            eIsbn: book['Electronic ISBN'],
            lang: book['Language'],
            package: book['English Package Name'],
            package_de: book['German Package Name'],
            doi: book['DOI URL'],
            url: book['OpenURL'],
            tags: (book['Subject Classification'] as string).split(';'),
            publisher: book['Publisher']
        }
    }    
}
