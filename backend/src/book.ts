export interface IBook {
    title: string;
    edition: string;
    type: string;
    year: number;
    isbn: string;
    eIsbn: string;
    lang: string;
    package: string;
    package_de: string;
    doi: string;
    url: string;
    tags: string[];
    publisher: string;
}